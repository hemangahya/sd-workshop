module.exports = {
    split: function (totalBill, people) {
        return Math.ceil(totalBill / people);
    },
};
